function builder(element, contents) {

    var container = document.getElementById(element);

    if(!container) {
        throw new Error("element with id " + element + " not exist")
    }

    if(!contents) {
        throw new Error("contents not defined")
    }

    if(typeof contents != 'object') {
        contents = [contents]
    }

    var frag = document.createFragmenElement()

    for(item of contents) {
        var el = document.createElement('div')
        el.innerHTML = item
        el.className = "block"
        frag.appendChild(el)
    }

    container.appendChild(frag)

    console.log(container, contents);
}
